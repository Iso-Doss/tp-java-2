# TP JAVA 2

Entité :
-Customer
-Country
-countryCode:String
-countryName:String
-Role
-roleName:Enum{"ROLE_USER","ROLE_ADMIN"}

Règles de gestion :
-un customer a un country
-un customer a un ou plusieurs rôles.

Tache :
-Créez les 3 entités en matérialisant correctement les relations entre elles.
-Crud customer , Crud country.
-Un endpoints permettant de retrouver un customer par son countryCode
-Un endpoints permettant de donner un rôle à un costumer
-Un endpoints permettant de retirer un rôle à un customer.
