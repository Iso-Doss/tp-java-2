package bj.isodoss.tpjava2.controller;

import bj.isodoss.tpjava2.model.Customer;
import bj.isodoss.tpjava2.model.Role;
import bj.isodoss.tpjava2.repository.CustomerRepository;
import bj.isodoss.tpjava2.repository.RoleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/customer")
public class CustomerController extends BaseController {

    @Autowired
    private CustomerRepository customerRepository;

    @Autowired
    private RoleRepository roleRepository;

    /**
     * Create - Add a customer.
     *
     * @param customer - A object of customer.
     * @return customer - A object of customer.
     */
    @PostMapping("")
    public Customer addCustomer(@RequestBody @Valid @Validated Customer customer) {
        customerRepository.save(customer);
        return customer;
    }

    /**
     * Read - Get a customer.
     *
     * @param customerId - A customer id.
     * @return customer - A object of customer.
     */
    @GetMapping("/{customerId}")
    public Customer getCustomer(@PathVariable(name = "customerId") Long customerId) {
        Optional<Customer> optionalCustomer = customerRepository.findById(customerId);
        Customer customer = new Customer();
        if (optionalCustomer.isPresent()) {
            customer = optionalCustomer.get();
        }
        return customer;
    }

    /**
     * Read - get a list of customer by country code.
     *
     * @return - A list of customer by country code.
     */
    @GetMapping("/country/{countryCode}")
    public List<Customer> getCustomerListByCountryCode(@PathVariable(name = "countryCode") String countryCode) {
        Optional<List<Customer>> optionalCustomerList = customerRepository.findByCountryCode(countryCode);
        List<Customer> customerList = new ArrayList<Customer>();
        if (optionalCustomerList.isPresent()) {
            customerList = optionalCustomerList.get();
        }
        return customerList;
    }

    /**
     * Update - Update a customer.
     *
     * @param customerId - A customer id.
     * @param customer   - A object of customer.
     * @return - A object of customer.
     */
    @PutMapping("/{customerId}")
    public Customer updateCustomer(@PathVariable(name = "customerId") Long customerId, @RequestBody Customer customer) {
        Optional<Customer> optionalCustomer = customerRepository.findById(customerId);
        Customer customerExist = new Customer();
        if (optionalCustomer.isPresent()) {
            customerExist = optionalCustomer.get();
            customerExist.setFirstName(customer.getFirstName());
            customerExist.setLastName(customer.getLastName());
            customerExist.setUserName(customer.getUserName());
            customerExist.setBirthday(customer.getBirthday());
            System.out.println(customerExist.getId());
            customerRepository.save(customerExist);
        }
        return customerExist;
    }

    /**
     * Delete - Delete a customer.
     *
     * @param customerId - A customer id.
     * @return - A object of customer.
     */
    @DeleteMapping("/{customerId}")
    public String deleteCustomer(@PathVariable(name = "customerId") Long customerId) {
        Optional<Customer> optionalCustomer = customerRepository.findById(customerId);
        if (optionalCustomer.isPresent()) {
            customerRepository.delete(optionalCustomer.get());
            return "L'utilisateur a été supprimé avec succès.";
        }
        return "L'utilisateur est introuvable";
    }

    /**
     * Add or remove roles of customer.
     *
     * @param customerId - A customer id.
     * @param roleId     - A role id.
     * @return - A object of customer.
     */
    @PutMapping("/add-remove-roles/{customerId}/{roleId}")
    public String addOrRemoveRole(@PathVariable(name = "customerId") Long customerId, @PathVariable(name = "roleId") Long roleId) {
        Optional<Role> optionalRole = roleRepository.findById(roleId);
        if (!optionalRole.isPresent()) {
            return "Ce role n'existe pas.";
        }
        List<Role> roleListExiste = new ArrayList<Role>();
        roleListExiste.add(optionalRole.get());
        Optional<Customer> optionalCustomer = customerRepository.findByIdAndRoleListIn(customerId, roleListExiste);
        if (optionalCustomer.isPresent()) {
            return "Ce role es deja attribué a ce utilisateur.";
        }
        Customer customer = customerRepository.findById(customerId).get();
        List<Role> roleList = customer.getRoleList();
        roleList.add(optionalRole.get());
        customer.setRoleList(roleList);
        customerRepository.save(customer);
        return "Le role a bien ete ajouté a ce utilisateur.";
    }

    /**
     * Read - get a list of customer.
     *
     * @return - A list of customer.
     */
    @GetMapping("")
    public List<Customer> getCustomerList() {
        List<Customer> customerList = new ArrayList<Customer>();
        customerList = customerRepository.findAll();
        return customerList;
    }

}
