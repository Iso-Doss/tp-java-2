package bj.isodoss.tpjava2.config;

import bj.isodoss.tpjava2.model.Role;
import bj.isodoss.tpjava2.model.RoleName;
import bj.isodoss.tpjava2.repository.RoleRepository;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

@Component
public class DataLoadConfig {
    private final RoleRepository roleRepository;


    public DataLoadConfig(RoleRepository roleRepository) {
        this.roleRepository = roleRepository;
    }

    @PostConstruct
    public void loadData() {
        loadRoles();
    }

    public void loadRoles() {
        for (RoleName roleName : RoleName.values()) {
            boolean exist = roleRepository.existsByName(roleName);
            if (!exist) {
                Role role = new Role();
                role.setName(roleName);
                roleRepository.save(role);
            }
        }
    }

}
