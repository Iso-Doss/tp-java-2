package bj.isodoss.tpjava2.model;

public enum RoleName {
    ROLE_USER,
    ROLE_ADMIN;
}
