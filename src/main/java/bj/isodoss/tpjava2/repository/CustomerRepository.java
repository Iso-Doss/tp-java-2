package bj.isodoss.tpjava2.repository;

import bj.isodoss.tpjava2.model.Customer;
import bj.isodoss.tpjava2.model.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface CustomerRepository extends JpaRepository<Customer, Long> {

    Optional<List<Customer>> findByCountryCode(String code);

    Optional<Customer> findByIdAndRoleListIn(Long customerId, List<Role> roleList);

}
