package bj.isodoss.tpjava2.repository;

import bj.isodoss.tpjava2.model.Role;
import bj.isodoss.tpjava2.model.RoleName;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RoleRepository extends JpaRepository<Role, Long> {

    boolean existsByName(RoleName roleName);
}
