package bj.isodoss.tpjava2.repository;

import bj.isodoss.tpjava2.model.Country;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CountryRepository  extends JpaRepository<Country, Long> {
}
